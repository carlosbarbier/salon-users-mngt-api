from flask_restful import inputs

from src.util import non_empty_string


def user_registration_validator(parser):
    parser.add_argument('name', help='valid name required', required=True, nullable=False, type=non_empty_string,
                        trim=True)
    parser.add_argument('email', help='valid email required',
                        nullable=False)
    parser.add_argument('password', help='valid password required', required=True, nullable=False,
                        type=non_empty_string)


def user_login_validator(parser):
    parser.add_argument('email', help='valid email required',
                        type=inputs.regex(r"(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)"), nullable=False)
    parser.add_argument('password', help='valid password required', required=True, nullable=False,
                        type=non_empty_string)


def email_validator(parser):
    parser.add_argument('email', help='valid email required',
                        type=inputs.regex(r"(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)"), nullable=False)


def user_reset_password_validator(parser):
    parser.add_argument('email', help='valid email required',
                        type=inputs.regex(r"(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)"), nullable=False)
    parser.add_argument('password', help='valid password required', required=True, nullable=False,
                        type=non_empty_string)
    parser.add_argument('confirm_code', type=str, help='confirm_code required', required=True, nullable=False)


def user_change_password_validator(parser):
    parser.add_argument('previous_password', help='valid password required', required=True, nullable=False,
                        type=non_empty_string)
    parser.add_argument('new_password', type=str, help='Current password required',
                        required=True, nullable=False)
    parser.add_argument('password_confirmation', type=str, help='Password confirmation required',
                        required=True, nullable=False)


def user_email_confirmation_validator(parser):
    parser.add_argument('email', help='valid email required',
                        type=inputs.regex(r"(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)"), nullable=False)
    parser.add_argument('confirm_code', type=str, help='token required', required=True, nullable=False)

