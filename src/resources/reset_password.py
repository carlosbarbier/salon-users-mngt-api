import http

from flask_restful import Resource, reqparse

from src import service
from src.validation import user_reset_password_validator


class ResetPasswordResource(Resource):

    def __init__(self):
        self.parser = reqparse.RequestParser(bundle_errors=True)

    def post(self):
        user_reset_password_validator(self.parser)
        args = self.parser.parse_args()
        service.reset_password(args=args)
        return {},http.HTTPStatus.OK
