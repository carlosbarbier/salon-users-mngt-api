from flask_restful import Resource, reqparse

from src import service
from src.validation import user_email_confirmation_validator


class EmailConfirmationResource(Resource):
    def __init__(self):
        self.parser = reqparse.RequestParser(bundle_errors=True)

    def post(self):
        user_email_confirmation_validator(self.parser)
        args = self.parser.parse_args()
        service.confirm_email(args)
        return {'message': 'confirm your email'}
