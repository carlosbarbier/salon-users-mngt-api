from flask_restful import Resource, reqparse

from src import service
from src.validation import email_validator


class ResendCodeResource(Resource):
    def __init__(self):
        self.parser = reqparse.RequestParser(bundle_errors=True)

    def post(self):
        email_validator(self.parser)
        args = self.parser.parse_args()
        service.resend_code(args)
        return {'message': 'confirm your email'}
