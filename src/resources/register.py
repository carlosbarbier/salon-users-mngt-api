from flask_restful import Resource, reqparse

from src import service
from src.util import auth
from src.validation import user_registration_validator


class RegisterUserResource(Resource):
    def __init__(self):
        self.parser = reqparse.RequestParser(bundle_errors=True)

    def post(self):
        user_registration_validator(self.parser)
        args = self.parser.parse_args(http_error_code=422)
        service.register(args)
        return {}, 201

    @auth()
    def get(self):
        # token = request.headers.get('Authorization')
        # access_token = token.replace('Bearer ', '')
        # return user_service.get_user_details(access_token=access_token)
        return {}
