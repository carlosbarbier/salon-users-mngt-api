from functools import wraps

import flask_restful
from flask import request
from werkzeug.exceptions import Unauthorized
from werkzeug.security import generate_password_hash

from src import cognito


def hash_password(password):
    return generate_password_hash(password)


def non_empty_string(s):
    if not s:
        raise ValueError("Must not be empty string")
    return s


def auth():
    def _auth(f):
        @wraps(f)
        def __auth(*args, **kwargs):
            if 'Authorization' in request.headers:
                token = request.headers.get('Authorization')
                if token is not None and len(token) > 10:
                    access_token = token.replace('Bearer ', '')
                    user = cognito.get_user(access_token=access_token)
                    if user is not None:
                        return f(*args, **kwargs)
                    flask_restful.abort(http_status_code=401, message="not authorized")
                else:
                    flask_restful.abort(http_status_code=401, message="Invalid or expired token")
            else:
                flask_restful.abort(http_status_code=400, message="Invalid or expired token")

        return __auth

    return _auth


def get_access_token(request):
    token = request.headers.get('Authorization')
    if token is not None and len(token) > 10:
        return token.replace('Bearer ', '')
    raise Unauthorized('Invalid or expired token')
