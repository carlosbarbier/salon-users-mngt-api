import uuid

from src import cognito


def register(args):
    user_id = uuid.uuid4().hex
    print(user_id)

    name = args['name']
    email = args['email']
    password = args['password']

    response = cognito.register_new_user(email=email, name=name, password=password)
    if response:
        user = {'id': user_id, 'name': name, 'email': email, 'password': password}
        print(user)


def login(args):
    email = args['email']
    password = args['password']

    response = cognito.authenticate_user(email=email, password=password)

    access_token = response["AuthenticationResult"]["AccessToken"]
    expires_in = response["AuthenticationResult"]["ExpiresIn"]
    refresh_token = response["AuthenticationResult"]["RefreshToken"]
    data = {"access_token": access_token, 'expires_in': expires_in, "refresh_token": refresh_token}

    return data


# def get_user(user_id: str):
#     return dynamo.get_by_id(user_id=user_id)


def get_user_details(access_token: str):
    return cognito.get_user(access_token=access_token)


def change_password(args, access_token: str):
    new_password = args['new_password']
    previous_password = args['previous_password']
    cognito.change_password(previous_password=previous_password, new_password=new_password, access_token=access_token)


def forgot_password(args):
    email = args['email']
    cognito.forgot_password(email)


def logout(access_token: str):
    cognito.logout(access_token=access_token)


def reset_password(args):
    email = args['email']
    confirm_code = args['confirm_code']
    password = args['password']
    cognito.reset_password(email=email, password=password, confirm_code=confirm_code)


def confirm_email(args):
    email = args['email']
    confirm_code = args['confirm_code']
    cognito.confirm_user_signup(email=email, confirm_code=confirm_code)


def resend_code(args):
    email = args['email']
    cognito.resend_confirmation_code(email=email)


def delete_user(access_token):
    cognito.delete_user_account(access_token)
