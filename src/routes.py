from flask_restful import Api

from src.resources.change_password import ChangePasswordResource
from src.resources.email_confirmation import EmailConfirmationResource
from src.resources.forgot_password import ForgotPasswordResource
from src.resources.get_user import GetUserResource
from src.resources.login import LoginUserResource
from src.resources.logout import LogoutResource
from src.resources.register import RegisterUserResource
from src.resources.resend_code import ResendCodeResource
from src.resources.reset_password import ResetPasswordResource


def register_routes(app):
    api = Api(app)

    api.add_resource(ForgotPasswordResource, '/api/users/password/forgot')
    api.add_resource(ResetPasswordResource, '/api/users/password/reset')
    api.add_resource(EmailConfirmationResource, '/api/users/email/confirmation')
    api.add_resource(ResendCodeResource, '/api/users/resend/code')
    api.add_resource(ChangePasswordResource, '/api/users/password/change')
    api.add_resource(GetUserResource, '/api/users/me')
    api.add_resource(RegisterUserResource, '/api/users/register')
    api.add_resource(LoginUserResource, '/api/users/login')
    api.add_resource(LogoutResource, '/api/users/logout')
