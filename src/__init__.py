from flask import Flask
from flask_cors import CORS

from src.exception import GlobalException
from src.routes import register_routes


def create_app():
    app = Flask(__name__)
    # route
    register_routes(app)
    # exception
    GlobalException.handle(app)
    # cors
    CORS(app, resources={r"/*": {"origins": "*"}})
    return app
