# Users(Staff) Management Api Developed with Serverless Framework

## URL

`https://olx54ums20.execute-api.us-east-1.amazonaws.com/api/users`

### Programming Language

`Python (version 3.8)`

### Prerequisites

In order to package your dependencies locally with `serverless-python-requirements`, you need to have `Python3.8`
installed locally. You can create and activate a dedicated virtual environment with the following command:

```bash
python3.8 -m venv ./venv
source ./venv/bin/activate
```

Also `Node js`, preferred version `>= 16.0`

### Prerequisites

In order to package your dependencies locally with `serverless-python-requirements`, you need to have `Python3.8`
installed locally. You can create and activate a dedicated virtual environment with the following command:

```bash
python3.8 -m venv ./venv
source ./venv/bin/activate
```

### Deployment

install dependencies with:

```
npm install
```

and

```
pip install -r requirements.txt
```

and then perform deployment with:

```
serverless deploy
```

After running deploy, you should see output similar to:

```bash
Deploying salon-notification-service-api to stage dev (us-east-1)

✔ Service deployed to stack salon-users-mngt-api-dev-api (182s)

endpoint: ANY - https://olx54ums20.execute-api.us-east-1.amazonaws.com
functions:
  api: salon-users-mngt-api-dev-api (1.5 MB)
```

_Note_: In current form, after deployment, your API is public and can be invoked by anyone. For production deployments,
you might want to configure an authorizer. For details on how to do that, refer
to [httpApi event docs](https://www.serverless.com/framework/docs/providers/aws/events/http-api/).

### Invocation

After successful deployment, you can call the created application via HTTP:

```bash
curl https://olx54ums20.execute-api.us-east-1.amazonaws.com/api
```

Which should result in the following response:

```
{"message":"route not found"}
```

Register a new account (we suggest using a real email):

### request

```bash
(POST) https://olx54ums20.execute-api.us-east-1.amazonaws.com/api/users/register
{
    "name": "Bob",
    "email": "carlos@gamil.com",
    "password": "123456789"
}
```

Should result in the following response:

```bash
{} 201 created
```

Login:

### request

```bash
(POST) https://olx54ums20.execute-api.us-east-1.amazonaws.com/api/users/login
{
    "email": "carlos@gamil.com",
    "password": "123456789"
}
```

Should result in the following response or bad request or forbidden  :

```bash
{
     "access_token": "access_token",
     "expires_in": 3600, 
     "refresh_token": "refresh_token"
 } 
```

Account confirmation:

### request

```bash
(POST) https://olx54ums20.execute-api.us-east-1.amazonaws.com/api/users/email/confirmation
{
    "email": "carlos@gamil.com",
    "confirm_code": 123456
}
```

Should result in the following response or bad request or not authorized  :

```bash
{} 200 ok
```

Forgot Password:

### request

```bash
(POST) https://olx54ums20.execute-api.us-east-1.amazonaws.com/api/users/password/forgot
{
    "email": "carlos@gamil.com",
}
```

Should result in the following response or bad request or not authorized  :

```bash
{} 200 ok
```

Change Password (make sure to pass the Bearer token which is in case the access_token):

### request

```bash
(POST) https://olx54ums20.execute-api.us-east-1.amazonaws.com/api/users/password/change

{
    "previous_password": "123456",
    "new_password": "654321",
    "password_confirmation":654321",
}
```

Should result in the following response :

```bash
{} 200 ok
```

Get Current login user (make sure to pass the Bearer token which is in case the access_token):

### request

```bash
(GET) https://olx54ums20.execute-api.us-east-1.amazonaws.com/api/users/me

```

Should result in the following response or not authorized :

```bash
{
    "id": "id2",
    "name": "kouakou",
    "email": "carlos.barbier2rok@outlook.com",
}
```

Logout the current user (make sure to pass the Bearer token which is in case the access_token):

### request

```bash
(POST) https://olx54ums20.execute-api.us-east-1.amazonaws.com/api/users/logout

```

Should result in the following response or not authorized :

```bash
{} 200 ok
```

Reset Password :

### request

```bash
(POST) https://olx54ums20.execute-api.us-east-1.amazonaws.com/api/users/password/reset
{
    "email": "carlos@gamil.com",
    "password": "654321",
    "confirm_code":654321,
}

```

Should result in the following response or not authorized or bad request :

```bash
{} 200 ok
```
Resend Code :

### request

```bash
(POST) https://olx54ums20.execute-api.us-east-1.amazonaws.com/api/users/code
{
    "email": "carlos@gamil.com",  
}

```

Should result in the following response or not authorized :

```bash
{} 200 ok
```
### Local development

Thanks to capabilities of `serverless-wsgi`, it is also possible to run your application locally, however, in order to
do that, you will need to first install `werkzeug` dependency, as well as all other dependencies listed
in `requirements.txt`. It is recommended to use a dedicated virtual environment for that purpose. You can install all
needed dependencies with the following commands:

```bash
pip install werkzeug
pip install -r requirements.txt
```

At this point, you can run your application locally with the following command:

```bash
serverless wsgi serve
```

